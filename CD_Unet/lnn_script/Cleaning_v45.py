"""
Created by Ine Dirks on 13/11/2020

"""

import os
import cv2
import pandas
import scipy
import statistics
import numpy as np
import pandas as pd
from random import *
from tqdm import tqdm
#import nibabel as nib
from numpy import load
import SimpleITK as sitk
from numpy import asarray
from statistics import mode
# import matplotlib.pyplot as plt
from numpy import savez_compressed
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
import numbers
from sklearn.utils import check_array, check_random_state
from numpy.lib.stride_tricks import as_strided
from itertools import product
from skimage.util.shape import view_as_blocks
from numpy import moveaxis

# TODO: changed
# data_pd = pd.read_excel("Dataset_&_lambda_GPU/dataset_properties_Laurent.xlsx")
data_pd = pd.read_excel("../dataset_properties_Laurent.xlsx")


# creating instance of labelencoder
labelencoder = LabelEncoder()

"""
# option1 for one hot encoding
# creating instance of one-hot-encoder
enc = OneHotEncoder(handle_unknown='ignore')
# passing bridge-types-cat column (label encoded values of bridge_types)
enc_df = pd.DataFrame(enc.fit_transform(preproc_df[['Hash']]).toarray())
# merge with main df preproc_df on key values
preproc_df = preproc_df.join(enc_df)


# option2 for one hot encoding
# generate binary values using get_dummies
dum_df = pd.get_dummies(preproc_df, columns=["Hash"], prefix=["Type_is"] )
# merge with main df preproc_df on key values
preproc_df = preproc_df.join(dum_df)
"""

# Properties
preproc_df = data_pd[['Case', 'Hash', 'Lesion file?']]
print(preproc_df.head())
print("----------------------------------------------------------------")
preproc_df = preproc_df.drop([0], axis=0)
print(preproc_df.head())
print("----------------------------------------------------------------")
# preproc_df['Hash_encoded'] = preproc_df['Hash'].astype('category')
# Assigning numerical values and storing in another column
preproc_df['Hash_encoded'] = labelencoder.fit_transform(preproc_df['Hash'])
print(preproc_df.head())
# plt.rcParams["figure.figsize"] = (20,10)
preproc_df['Hash_encoded'].plot(kind='hist', bins = 69)
preproc_df.groupby(['Hash_encoded','Lesion file?']).size().unstack().plot(kind='bar',stacked=True)
# plt.savefig('lesion_dist_per_patient0.png')

# insert a column indicating the number of lesion per case
NaN = np.nan
preproc_df["nberOfLesions"] = NaN
print(preproc_df.head())

df = preproc_df.set_index("Case", drop = False)
print(df.head())

df['nberOfLesions'] = df['nberOfLesions'].fillna(0)
print(df.head())

df["Size_PET"] = NaN
df["Size_CT"] = NaN
df["Size_LESION"] = NaN
#*****************************************************#
df["Size_PET"] = df["Size_PET"].fillna(0)
df["Size_CT"] = df["Size_CT"].fillna(0)
df["Size_LESION"] = df["Size_LESION"].fillna(0)
df["Size_PET"] = NaN
df["Size_CT"] = NaN
df["Size_LESION"] = NaN
#*****************************************************#
df["Size_PET"] = df["Size_PET"].fillna(0)
df["Size_CT"] = df["Size_CT"].fillna(0)
df["Size_LESION"] = df["Size_LESION"].fillna(0)
df['Patch_indexes'] = NaN #np.empty((len(df), 0)).tolist()
# df.drop(columns=['empty_list'], inplace = True)
print(df.head())
df['Patch_indexes'] = NaN #np.empty((len(df), 0)).tolist()
# df.drop(columns=['empty_list'], inplace = True)
print(df.head())

# to have the borders, we take two extreme points of the image box(cube)
# origin point in pixel world: init(0,0,0) - xtrem point in pixel world: xtrem(img.GetSize())
# index_pt = img.TransformPhysicalPointToIndex(phys_pt)
# phys_pt = img.TransformIndexToPhysicalPoint(index_pt)

def img_xtrem_pts(ref_img):
    # get the physical coordinates of the extreme points of an image: origin:(0,0,0) and xtrem:width(x), depth(y), height(z)
    origin_phys_pt = ref_img.GetOrigin()
    origin_index_pt = ref_img.TransformPhysicalPointToIndex(origin_phys_pt)
    xtrem_index_pt = ref_img.GetSize()
    xtrem_phys_pt = ref_img.TransformIndexToPhysicalPoint(xtrem_index_pt)
    return origin_phys_pt, xtrem_phys_pt


def img_xtrem_index_pts(ref_img, origin_phys_pt, xtrem_phys_pt):
    origin_index_pt = ref_img.TransformPhysicalPointToIndex(origin_phys_pt)
    xtrem_index_pt = ref_img.TransformPhysicalPointToIndex(xtrem_phys_pt)
    return origin_index_pt, xtrem_index_pt


def extract_region_to_crop(origin_phys_pt_1, xtrem_phys_pt_1, origin_phys_pt_2, xtrem_phys_pt_2):
    # retrieve the region corresponding to the common physical area of two 3D images (ct and pet) of the same patient
    if len(origin_phys_pt_1) != len(origin_phys_pt_2):
        raise ValueError("Dimension of the origin coordinates should be the same.")
    if len(xtrem_phys_pt_1) != len(xtrem_phys_pt_2):
        raise ValueError("Dimension of the extreme coordinates should be the same.")

    list_origin_phys_pt_cropped = [max(origin_phys_pt_1[i], origin_phys_pt_2[i]) for i in range(len(origin_phys_pt_1))]
    origin_phys_pt_cropped = tuple(list_origin_phys_pt_cropped)
    #
    list_xtrem_phys_pt_cropped = [min(xtrem_phys_pt_1[i], xtrem_phys_pt_2[i]) for i in range(len(xtrem_phys_pt_1))]

    xtrem_phys_pt_cropped = tuple(list_xtrem_phys_pt_cropped)

    return origin_phys_pt_cropped, xtrem_phys_pt_cropped


def crop_image(img, origin_phys_pt_cropped, xtrem_phys_pt_cropped):
    # crop a 3D volume using slicing
    origin_index_pt, xtrem_index_pt = img_xtrem_index_pts(img, origin_phys_pt_cropped, xtrem_phys_pt_cropped)
    img_cropped = img[origin_index_pt[0]:xtrem_index_pt[0], origin_index_pt[1]:xtrem_index_pt[1],
                  origin_index_pt[2]:xtrem_index_pt[2]]

    return img_cropped


def resample_image(image, reference_image, new_spacing, label_image):
    # print('   resampling image...')
    resampler = sitk.ResampleImageFilter()
    if label_image:
        resampler.SetInterpolator(sitk.sitkNearestNeighbor)  # Nearest neighbor for segmentation images
    else:
        resampler.SetInterpolator(sitk.sitkLinear)  # Linear for intensity images
    resampler.SetOutputDirection(reference_image.GetDirection())
    resampler.SetOutputOrigin(reference_image.GetOrigin())
    resampler.SetOutputSpacing(new_spacing)
    # take the minimum value in the image as default pixel value
    min_filter = sitk.MinimumMaximumImageFilter()
    min_filter.Execute(image)
    resampler.SetDefaultPixelValue(min_filter.GetMinimum())
    #     resampler.SetDefaultPixelValue(-1000)
    orig_size = np.array(reference_image.GetSize())  # [320, 320, 320]
    orig_spacing = np.array(reference_image.GetSpacing())
    new_size = np.around(orig_size * (orig_spacing / new_spacing))
    new_size = [int(s) for s in new_size]  # [320, 320, 320]
    new_size = [s + (32 - s % 32) for s in new_size]  # to go to the next multiple of 32
    resampler.SetSize(new_size)
    resampled_image = resampler.Execute(image)

    return resampled_image


def random_rotation_3d(batch_pet, batch_ct, mask, angle=45):
    """ Randomly rotate an image by a random angle (-max_angle, max_angle).

    Arguments:
    max_angle: `float`. The maximum rotation angle.

    Returns:
    batch of rotated 3D images
    """
    size = mask.shape

    if size != batch_ct.shape or size != batch_pet.shape:
        raise ValueError("Incoherent shape between input modalities")
    # print("shape of input batch: ", size)

    if len(size) > 4:
        batch_pet = np.squeeze(batch_pet)
        batch_ct = np.squeeze(batch_ct)
        mask = np.squeeze(mask)

    # print("mask ndim: ", mask.ndim)
    # print("mask shape: ", mask.shape)

    batch_pet_rot1 = np.zeros(batch_pet.shape)
    batch_pet_rot2 = np.zeros(batch_pet.shape)
    batch_pet_rot3 = np.zeros(batch_pet.shape)

    batch_ct_rot1 = np.zeros(batch_ct.shape)
    batch_ct_rot2 = np.zeros(batch_ct.shape)
    batch_ct_rot3 = np.zeros(batch_ct.shape)

    mask_rot1 = np.zeros(mask.shape)
    mask_rot2 = np.zeros(mask.shape)
    mask_rot3 = np.zeros(mask.shape)

    for i in range(mask.shape[0]):
        if True:
            mask1 = mask[i]
            batch_ct1 = batch_ct[i]
            batch_pet1 = batch_pet[i]

            """ 
            if i == 0:
                vf = 0
                print("mask1 ndim: ", mask1.ndim)
                print("mask1 shape: ", mask1.shape)
            """
            # rotate along z-axis
            # angle = random.uniform(-max_angle, max_angle)
            mask_rot1[i] = scipy.ndimage.interpolation.rotate(mask1, angle, mode='nearest', axes=(0, 1), reshape=False)
            batch_ct_rot1[i] = scipy.ndimage.interpolation.rotate(batch_ct1, angle, mode='nearest', axes=(0, 1),
                                                                  reshape=False)
            batch_pet_rot1[i] = scipy.ndimage.interpolation.rotate(batch_pet1, angle, mode='nearest', axes=(0, 1),
                                                                   reshape=False)

            # rotate along y-axis
            # angle = random.uniform(-max_angle, max_angle)
            mask_rot2[i] = scipy.ndimage.interpolation.rotate(mask1, angle, mode='nearest', axes=(0, 2), reshape=False)
            batch_ct_rot2[i] = scipy.ndimage.interpolation.rotate(batch_ct1, angle, mode='nearest', axes=(0, 2),
                                                                  reshape=False)
            batch_pet_rot2[i] = scipy.ndimage.interpolation.rotate(batch_pet1, angle, mode='nearest', axes=(0, 2),
                                                                   reshape=False)

            # rotate along x-axis
            # angle = random.uniform(-max_angle, max_angle)
            mask_rot3[i] = scipy.ndimage.interpolation.rotate(mask1, angle, mode='nearest', axes=(1, 2), reshape=False)
            batch_ct_rot3[i] = scipy.ndimage.interpolation.rotate(batch_ct1, angle, mode='nearest', axes=(1, 2),
                                                                  reshape=False)
            batch_pet_rot3[i] = scipy.ndimage.interpolation.rotate(batch_pet1, angle, mode='nearest', axes=(1, 2),
                                                                   reshape=False)
            # print(i)
        #
        #
    return [batch_pet_rot1.reshape(size), batch_pet_rot2.reshape(size), batch_pet_rot3.reshape(size)], [
        batch_ct_rot1.reshape(size), batch_ct_rot2.reshape(size), batch_ct_rot3.reshape(size)], [
               mask_rot1.reshape(size), mask_rot2.reshape(size), mask_rot3.reshape(size)]


def flipping_3d(batch_pet, batch_ct, mask):
    """ flipping an image around the three axis.

    Arguments:
    None

    Returns:
    batch of rotated 3D images
    """
    size = mask.shape

    if size != batch_ct.shape or size != batch_pet.shape:
        raise ValueError("Incoherent shape between input modalities")

    if len(size) > 4:
        batch_pet = np.squeeze(batch_pet)
        batch_ct = np.squeeze(batch_ct)
        mask = np.squeeze(mask)

    batch_pet_flip1 = np.zeros(batch_pet.shape)
    batch_pet_flip2 = np.zeros(batch_pet.shape)
    batch_pet_flip3 = np.zeros(batch_pet.shape)

    batch_ct_flip1 = np.zeros(batch_ct.shape)
    batch_ct_flip2 = np.zeros(batch_ct.shape)
    batch_ct_flip3 = np.zeros(batch_ct.shape)

    mask_flip1 = np.zeros(mask.shape)
    mask_flip2 = np.zeros(mask.shape)
    mask_flip3 = np.zeros(mask.shape)

    for i in range(mask.shape[0]):
        if True:
            batch_pet1 = batch_pet[i]
            batch_ct1 = batch_ct[i]
            mask1 = mask[i]

            # rotate along z-axis
            # angle = random.uniform(-max_angle, max_angle)
            batch_pet_flip1[i] = batch_pet1[::-1, :, :]
            batch_ct_flip1[i] = batch_ct1[::-1, :, :]
            mask_flip1[i] = mask1[::-1, :, :]

            # rotate along y-axis
            # angle = random.uniform(-max_angle, max_angle)
            batch_pet_flip2[i] = batch_pet1[:, ::-1, :]
            batch_ct_flip2[i] = batch_ct1[:, ::-1, :]
            mask_flip2[i] = mask1[:, ::-1, :]

            # rotate along x-axis
            # angle = random.uniform(-max_angle, max_angle)
            batch_pet_flip3[i] = batch_pet1[:, :, ::-1]
            batch_ct_flip3[i] = batch_ct1[:, :, ::-1]
            mask_flip3[i] = mask1[:, :, ::-1]
            # print(i)

    return [batch_pet_flip1.reshape(size), batch_pet_flip2.reshape(size), batch_pet_flip3.reshape(size)], [
        batch_ct_flip1.reshape(size), batch_ct_flip2.reshape(size), batch_ct_flip3.reshape(size)], [
               mask_flip1.reshape(size), mask_flip2.reshape(size), mask_flip3.reshape(size)]


""" changed this """
# DATA_PATH = os.path.join(os.path.dirname(os.getcwd()), 'Dataset_rescaledzx0')  # '/path/to/your/data_dir'
DATA_PATH = os.path.join('/Volumes/Passport/Laurent', 'Dataset_rescaledzx0')

# Create folders to hold images and masks between the different categories (train - validation - test*)
for fold in ["train", "validation"]:
    pathToBeCreated0 = os.path.join(DATA_PATH, fold)
    if not os.path.exists(pathToBeCreated0):
        os.makedirs(pathToBeCreated0)

    for folder in ['data_frames_pet', 'data_frames_ct', 'data_frames_lesion']:
        pathToBeCreated1 = os.path.join(pathToBeCreated0, folder)
        if not os.path.exists(pathToBeCreated1):
            os.makedirs(pathToBeCreated1)

dico = {"Hashes": {}, "Lesion_indexes": {}, "Size_PET": {}, "Size_CT": {}, "Size_LESION": {}}
dico["Hashes"] = {Hash: 0 for Hash in df['Hash'].unique().tolist()}
glo_index = 0

"""
max_list_pet = []
min_list_ct = []
max_list_ct = []
min_list_pet = []


x_train = []
y_train = [] 
"""


# max_pet = max(max_list_pet)
# min_ct = min(min_list_ct)
# max_ct = max(max_list_ct)
# min_pet = min(min_list_pet)
#
# print("min_pet = ", min_pet)
# print("min_ct = ", min_ct)
# print("max_pet = ", max_pet)
# print("max_ct = ", max_ct)

### Stat training set:

min_pet = 0.0
min_ct = -1024.0
max_pet = 192.43629700266936
max_ct = 3071.0

seed(8) #10,

# we define the portion of each set: train, validation & test
nr_patient = 69
nr_patient_test = nr_patient//12
nr_patient_train = (nr_patient*5)//6
nr_patient_val = 69 - nr_patient_train - nr_patient_test
train = sample(range(69), nr_patient_train)
validation = sample([i for i in range(69) if i not in train], nr_patient_val)
test = [ind for ind in range(69) if ind not in train and ind not in validation]

print(train)
print(validation)
print(test)

""" this was added """
clip_pet = [0, 70]
clip_ct = [-1000, 1000]  # air = -1000 HU, dense bone = 1000+
rescale_pet = [0, 1]
rescale_ct = [0, 1]

all_pet_max = []
for (root, dirs, files) in os.walk(r"/Users/ine/Documents/Melanoma/Data", topdown=True):
    if "case" in root and "Lesions.nii.gz" in files:

        case_nr = root.split("_")[-1]

        if int(case_nr) in train and int(case_nr) not in [64, 149]:
            pet_image = sitk.ReadImage(root + '/PET_SUV_bw.nii.gz')
            filter = sitk.MinimumMaximumImageFilter()
            filter.Execute(pet_image)
            all_pet_max.append(filter.GetMaximum())
max_pet = max(all_pet_max)
# print(max_pet)
# print([i for i, j in enumerate(all_pet_max) if j == max_pet])

def rescale(x, xmax, xmin):
    y = (x - xmin) / (xmax - xmin)
    return y


def normalize(image_array, clip, rescale):
    min_bound = clip[0]
    max_bound = clip[1]
    image_array = (image_array - min_bound) / (max_bound - min_bound)
    image_array[image_array > 1] = rescale[0]
    image_array[image_array < 0] = rescale[1]

    return image_array


def zero_center(image_array, pixel_mean):
    image_array = image_array - pixel_mean

    return image_array

print("------------------------------------------------------")
import pickle
with open('./dataset_split.pkl', 'rb') as file:
    dataset_split = pickle.load(file)
    train_set = dataset_split['train']
    validation_set = dataset_split['validation']
    test_set = dataset_split['test']

""" end of this was added """

# for (root, dirs, files) in os.walk(r"C:\Users\admin\Downloads\VUB\Thesis MACS\Soft Work Practice\EtroDataset",
#                                    topdown=True):
for (root, dirs, files) in os.walk(r"/Users/ine/Documents/Melanoma/Data", topdown=True):
    if "case" in root and "Lesions.nii.gz" in files:

        case_nr = root.split("_")[-1]

        if int(case_nr) not in [64, 149]:
            print("------------------------------------------------------")

            """ changed this """
            if int(case_nr) in train_set:
                DATA_PATHx = os.path.join(DATA_PATH, "train")
                print("**********training************")
            elif int(case_nr) in validation_set:
                DATA_PATHx = os.path.join(DATA_PATH, "validation")
                print("**********validation************")
            else:
                DATA_PATHx = ""
                continue
            """ end of changed this """

            # if df.loc["case_" + case_nr, "Hash_encoded"] in train:
            #     DATA_PATHx = os.path.join(DATA_PATH, "train")
            #     print("**********training************")
            # elif df.loc["case_" + case_nr, "Hash_encoded"] in validation:
            #     DATA_PATHx = os.path.join(DATA_PATH, "validation")
            #     print("**********validation************")
            # else:
            #     DATA_PATHx = ""
            #     continue

            # DATA_PATHx = os.path.join(DATA_PATH, "train")
            # print("Working on case #", case_nr)
            # path of the target case
            data_path = "case_" + case_nr

            patient_code = df.loc[data_path, "Hash_encoded"]
            print("Working on case #", case_nr, " patient $", patient_code)

            """ changed this """
            # ref_img_pet = sitk.ReadImage(os.path.join(data_path, 'PET_SUV_bw.nii.gz'))  # PET.nii.gz , Lesions.nii.gz
            # ref_img_ct = sitk.ReadImage(os.path.join(data_path, 'CT.nii.gz'))
            # ref_img_label = sitk.ReadImage(os.path.join(data_path, 'Lesions.nii.gz'))

            ref_img_pet = sitk.ReadImage(os.path.join(root, 'PET_SUV_bw.nii.gz'))  # PET.nii.gz , Lesions.nii.gz
            ref_img_ct = sitk.ReadImage(os.path.join(root, 'CT.nii.gz'))
            ref_img_label = sitk.ReadImage(os.path.join(root, 'Lesions.nii.gz'))

            # pet image xtrem point
            origin_phys_pt_pet, xtrem_phys_pt_pet = img_xtrem_pts(ref_img_pet)
            # print("origin of the pet image: ", origin_phys_pt_pet)
            # print("extremes of the pet image: ", xtrem_phys_pt_pet)

            # ct image xtrem point
            origin_phys_pt_ct, xtrem_phys_pt_ct = img_xtrem_pts(ref_img_ct)
            # print("origin of the ct image: ", origin_phys_pt_ct)
            # print("extremes of the ct image: ", xtrem_phys_pt_ct)

            # return the two points of the region to crop
            origin_phys_pt_cropped, xtrem_phys_pt_cropped = extract_region_to_crop(origin_phys_pt_pet,
                                                                                   xtrem_phys_pt_pet,
                                                                                   origin_phys_pt_ct, xtrem_phys_pt_ct)
            # print("origin of the cropped image: ", origin_phys_pt_cropped)
            # print("extremes of the cropped image: ", xtrem_phys_pt_cropped)

            # crop the images(pet, ct and label) to the same physical size
            cropped_img_pet = crop_image(ref_img_pet, origin_phys_pt_cropped, xtrem_phys_pt_cropped)
            cropped_img_ct = crop_image(ref_img_ct, origin_phys_pt_cropped, xtrem_phys_pt_cropped)
            cropped_img_label = crop_image(ref_img_label, origin_phys_pt_cropped, xtrem_phys_pt_cropped)

            # resample images
            PET_img_resampled = resample_image(image=cropped_img_pet,
                                               reference_image=cropped_img_pet,
                                               new_spacing=(4, 4, 4),
                                               label_image=False)
            dico["Size_PET"][data_path] = PET_img_resampled.GetSize()
            # print(dico["Size_PET"][data_path])
            nr_patches_pet = int(np.prod(list(dico["Size_PET"][data_path])) / 32768)
            # sitk.WriteImage(PET_img_resampled, os.path.join(data_path, "PET_img_resampled2.nii.gz"))

            Lesions_img_resampled = resample_image(image=cropped_img_label,
                                                   reference_image=cropped_img_label,
                                                   new_spacing=(4, 4, 4),
                                                   label_image=True)
            dico["Size_LESION"][data_path] = Lesions_img_resampled.GetSize()
            nr_patches_lesion = int(np.prod(list(dico["Size_LESION"][data_path])) / 32768)
            # sitk.WriteImage(Lesions_img_resampled, os.path.join(data_path, "Lesions_img_resampled2.nii.gz"))

            CT_img_resampled = resample_image(image=cropped_img_ct,
                                              reference_image=cropped_img_ct,
                                              new_spacing=(4, 4, 4),
                                              label_image=False)
            dico["Size_CT"][data_path] = CT_img_resampled.GetSize()
            nr_patches_ct = int(np.prod(list(dico["Size_CT"][data_path])) / 32768)
            # sitk.WriteImage(CT_img_resampled, os.path.join(data_path, "CT_img_resampled2.nii.gz"))

            # sitk.WriteImage(cropped_img_pet, './cropped_pet.nii.gz')
            # sitk.WriteImage(cropped_img_ct, './cropped_ct.nii.gz')
            # sitk.WriteImage(cropped_img_label, './cropped_label.nii.gz')
            #
            # sitk.WriteImage(PET_img_resampled, './resampled_pet.nii.gz')
            # sitk.WriteImage(CT_img_resampled, './resampled_ct.nii.gz')
            # sitk.WriteImage(Lesions_img_resampled, './resampled_label.nii.gz')

            # read images into numpy array
            PET_arr_resampled = sitk.GetArrayFromImage(PET_img_resampled)
            CT_arr_resampled = sitk.GetArrayFromImage(CT_img_resampled)
            Lesions_arr_resampled = sitk.GetArrayFromImage(Lesions_img_resampled)

            """ changed this """
            # rescale image
            # PET_arr_resampledx = rescale(PET_arr_resampled, max_pet, min_pet)
            # CT_arr_resampledx = rescale(CT_arr_resampled, max_ct, min_ct)

            PET_arr_resampledx = normalize(PET_arr_resampled, clip_pet, rescale_pet)
            CT_arr_resampledx = normalize(CT_arr_resampled, clip_ct, rescale_ct)
            """ end of changed this """

            # plt.imshow(PET_arr_resampledx[:,64,:], cmap=plt.cm.gray)

            # extract patches from pet & lesion images

            PET_arr_long = view_as_blocks(PET_arr_resampledx, block_shape=(32, 32, 32))
            arr_pet = []
            (Ip, Jp, Kp) = PET_arr_long.shape[:3]
            for k in range(Kp):
                for j in range(Jp):
                    for i in range(Ip):
                        arr_pet.append(PET_arr_long[i, j, k, :, :, :])
            res_arr_pet = np.array(arr_pet)
            # print(res_arr_pet.shape)

            CT_arr_long = view_as_blocks(CT_arr_resampledx, block_shape=(32, 32, 32))
            arr_ct = []
            (Ic, Jc, Kc) = CT_arr_long.shape[:3]
            for k in range(Kc):
                for j in range(Jc):
                    for i in range(Ic):
                        arr_ct.append(CT_arr_long[i, j, k, :, :, :])
            res_arr_ct = np.array(arr_ct)

            Lesions_arr_long = view_as_blocks(Lesions_arr_resampled, block_shape=(32, 32, 32))
            arr_les = []
            (Il, Jl, Kl) = Lesions_arr_long.shape[:3]
            for k in range(Kl):
                for j in range(Jl):
                    for i in range(Il):
                        arr_les.append(Lesions_arr_long[i, j, k, :, :, :])
            res_arr_les = np.array(arr_les)

            # sitk.WriteImage(sitk.GetImageFromArray(res_arr_pet), './res_arr_pet.nii.gz')
            # sitk.WriteImage(sitk.GetImageFromArray(res_arr_ct), './res_arr_ct.nii.gz')
            # sitk.WriteImage(sitk.GetImageFromArray(res_arr_les), './res_arr_les.nii.gz')

            # get the index of the patches containing lesions
            lesion_indexes = [i for i in range(res_arr_les.shape[0]) if int(1) in res_arr_les[i]]
            # get the index of the patches not containing lesions
            nonlesion_indexes = [j for j in range(res_arr_les.shape[0]) if j not in lesion_indexes]
            # choose an equal number of patches without lesion to include in the training set
            sample_nonlesion_indexes = sample(nonlesion_indexes, len(lesion_indexes))

            print(res_arr_pet.shape)
            print(res_arr_ct.shape)
            print(res_arr_les.shape)
            print(lesion_indexes)
            print(nonlesion_indexes)

            if len(lesion_indexes) == 0:
                print("No patch with lesions found")
            else:
                # store the number of patches with lesion for this case
                df.loc[data_path, "nberOfLesions"] = len(lesion_indexes)
                print("number of patches with lesions: ", len(lesion_indexes))
                print("indexes of the patches with lesions: ", lesion_indexes)
                print("indexes of the selected patches without lesions: ", sample_nonlesion_indexes)
                # df.loc[data_path,"Patch_indexes"] = lesion_indexes
                dico["Lesion_indexes"][data_path] = lesion_indexes
                # increment the total number of patches for this patient (identify through the unique hash)
                dico["Hashes"][df.loc[data_path, "Hash"]] += len(lesion_indexes)

                """ changed this """
                # apply transformation to data for augmentation
                # [pet_rz, pet_ry, pet_rx], [ct_rz, ct_ry, ct_rx], [mask_rz, mask_ry, mask_rx] = random_rotation_3d(
                #     res_arr_pet[lesion_indexes], res_arr_ct[lesion_indexes], res_arr_les[lesion_indexes], angle=45)
                # [pet_fz, pet_fy, pet_fx], [ct_fz, ct_fy, ct_fx], [mask_fz, mask_fy, mask_fx] = flipping_3d(
                #     res_arr_pet[lesion_indexes], res_arr_ct[lesion_indexes], res_arr_les[lesion_indexes])
                mask = res_arr_les[lesion_indexes]
                batch_ct = res_arr_ct[lesion_indexes]
                batch_pet = res_arr_pet[lesion_indexes]
                from data_augmentation import augment_data

                for i in range(len(lesion_indexes)):
                    mask1 = mask[i]
                    batch_ct1 = batch_ct[i]
                    batch_pet1 = batch_pet[i]

                    print(batch_ct1.shape)

                    rotated_ct_batch, translated_ct_batch, flipped_ct_batch = augment_data(batch_ct1)
                    rotated_pet_batch, translated_pet_batch, flipped_pet_batch = augment_data(batch_pet1)
                    rotated_mask_batch, translated_mask_batch, flipped_mask_batch = augment_data(mask1)

                # for i in range(len(lesion_indexes)):
                    print('Lesion index:', i)

                    ''' 
                    # apply transformation to data for augmentation
                    [pet_rz, pet_ry, pet_rx], [ct_rz, ct_ry, ct_rx], [mask_rz, mask_ry, mask_rx] = random_rotation_3d(rescale(res_arr_pet[lesion_indexes[i]], max_pet, min_pet), rescale(res_arr_ct[lesion_indexes[i]], max_ct, min_ct), res_arr_les[lesion_indexes[i]], angle = 45)
                    [pet_fz, pet_fy, pet_fx], [ct_fz, ct_fy, ct_fx], [mask_fz, mask_fy, mask_fx] = flipping_3d(rescale(res_arr_pet[lesion_indexes[i]], max_pet, min_pet), rescale(res_arr_ct[lesion_indexes[i]], max_ct, min_ct), res_arr_les[lesion_indexes[i]])
                    '''
                    # pet
                    " "
                    for rot_nr in range(len(rotated_pet_batch)):
                        np.save(os.path.join(os.path.join(DATA_PATHx, 'data_frames_pet'),
                                             str(patient_code) + '_' + str(case_nr) + '_l_rot' + str(rot_nr) + '_' + str(
                                                 lesion_indexes[i]) + '_' + str(glo_index)), rotated_pet_batch[rot_nr])

                    for transl_nr in range(len(translated_pet_batch)):
                        np.save(os.path.join(os.path.join(DATA_PATHx, 'data_frames_pet'),
                                             str(patient_code) + '_' + str(case_nr) + '_l_transl' + str(transl_nr) + '_' + str(
                                                 lesion_indexes[i]) + '_' + str(glo_index)), translated_pet_batch[transl_nr])

                    for flip_nr in range(len(flipped_pet_batch)):
                        np.save(os.path.join(os.path.join(DATA_PATHx, 'data_frames_pet'),
                                             str(patient_code) + '_' + str(case_nr) + '_l_flip' + str(flip_nr) + '_' + str(
                                                 lesion_indexes[i]) + '_' + str(glo_index)), flipped_pet_batch[flip_nr])

                    print(os.path.join(os.path.join(DATA_PATHx, 'data_frames_pet'),
                                       str(patient_code) + '_' + str(case_nr) + '_l_rotz_' + str(lesion_indexes[i]) + '_' + str(glo_index)))
                    #
                    # np.save(os.path.join(os.path.join(DATA_PATHx, 'data_frames_pet'),
                    #                      str(patient_code) + '_' + str(case_nr) + '_l_rotz_' + str(
                    #                          lesion_indexes[i]) + '_' + str(glo_index)), pet_rz[i])
                    # np.save(os.path.join(os.path.join(DATA_PATHx, 'data_frames_pet'),
                    #                      str(patient_code) + '_' + str(case_nr) + '_l_roty_' + str(
                    #                          lesion_indexes[i]) + '_' + str(glo_index)), pet_ry[i])
                    # np.save(os.path.join(os.path.join(DATA_PATHx, 'data_frames_pet'),
                    #                      str(patient_code) + '_' + str(case_nr) + '_l_rotx_' + str(
                    #                          lesion_indexes[i]) + '_' + str(glo_index)), pet_rx[i])

                    # np.save(os.path.join(os.path.join(DATA_PATHx, 'data_frames_pet'),
                    #                      str(patient_code) + '_' + str(case_nr) + '_l_flipz_' + str(
                    #                          lesion_indexes[i]) + '_' + str(glo_index)), pet_fz[i])
                    # np.save(os.path.join(os.path.join(DATA_PATHx, 'data_frames_pet'),
                    #                      str(patient_code) + '_' + str(case_nr) + '_l_flipy_' + str(
                    #                          lesion_indexes[i]) + '_' + str(glo_index)), pet_fy[i])
                    # np.save(os.path.join(os.path.join(DATA_PATHx, 'data_frames_pet'),
                    #                      str(patient_code) + '_' + str(case_nr) + '_l_flipx_' + str(
                    #                          lesion_indexes[i]) + '_' + str(glo_index)), pet_fx[i])

                    # ct
                    for rot_nr in range(len(rotated_ct_batch)):
                        np.save(os.path.join(os.path.join(DATA_PATHx, 'data_frames_ct'),
                                             str(patient_code) + '_' + str(case_nr) + '_l_rot' + str(rot_nr) + '_' + str(
                                                 lesion_indexes[i]) + '_' + str(glo_index)), rotated_ct_batch[rot_nr])

                    for transl_nr in range(len(translated_ct_batch)):
                        np.save(os.path.join(os.path.join(DATA_PATHx, 'data_frames_ct'),
                                             str(patient_code) + '_' + str(case_nr) + '_l_transl' + str(transl_nr) + '_' + str(
                                                 lesion_indexes[i]) + '_' + str(glo_index)), translated_ct_batch[transl_nr])

                    for flip_nr in range(len(flipped_ct_batch)):
                        np.save(os.path.join(os.path.join(DATA_PATHx, 'data_frames_ct'),
                                             str(patient_code) + '_' + str(case_nr) + '_l_flip' + str(
                                                 flip_nr) + '_' + str(
                                                 lesion_indexes[i]) + '_' + str(glo_index)), flipped_ct_batch[flip_nr])

                    # np.save(os.path.join(os.path.join(DATA_PATHx, 'data_frames_ct'),
                    #                      str(patient_code) + '_' + str(case_nr) + '_l_rotz_' + str(
                    #                          lesion_indexes[i]) + '_' + str(glo_index)), ct_rz[i])
                    # np.save(os.path.join(os.path.join(DATA_PATHx, 'data_frames_ct'),
                    #                      str(patient_code) + '_' + str(case_nr) + '_l_roty_' + str(
                    #                          lesion_indexes[i]) + '_' + str(glo_index)), ct_ry[i])
                    # np.save(os.path.join(os.path.join(DATA_PATHx, 'data_frames_ct'),
                    #                      str(patient_code) + '_' + str(case_nr) + '_l_rotx_' + str(
                    #                          lesion_indexes[i]) + '_' + str(glo_index)), ct_rx[i])
                    #
                    # np.save(os.path.join(os.path.join(DATA_PATHx, 'data_frames_ct'),
                    #                      str(patient_code) + '_' + str(case_nr) + '_l_flipz_' + str(
                    #                          lesion_indexes[i]) + '_' + str(glo_index)), ct_fz[i])
                    # np.save(os.path.join(os.path.join(DATA_PATHx, 'data_frames_ct'),
                    #                      str(patient_code) + '_' + str(case_nr) + '_l_flipy_' + str(
                    #                          lesion_indexes[i]) + '_' + str(glo_index)), ct_fy[i])
                    # np.save(os.path.join(os.path.join(DATA_PATHx, 'data_frames_ct'),
                    #                      str(patient_code) + '_' + str(case_nr) + '_l_flipx_' + str(
                    #                          lesion_indexes[i]) + '_' + str(glo_index)), ct_fx[i])

                    # lesion
                    for rot_nr in range(len(rotated_mask_batch)):
                        np.save(os.path.join(os.path.join(DATA_PATHx, 'data_frames_lesion'),
                                             str(patient_code) + '_' + str(case_nr) + '_l_rot' + str(rot_nr) + '_' + str(
                                                 lesion_indexes[i]) + '_' + str(glo_index)), rotated_mask_batch[rot_nr])

                    for transl_nr in range(len(translated_mask_batch)):
                        np.save(os.path.join(os.path.join(DATA_PATHx, 'data_frames_lesion'),
                                             str(patient_code) + '_' + str(case_nr) + '_l_transl' + str(transl_nr) + '_' + str(
                                                 lesion_indexes[i]) + '_' + str(glo_index)), translated_mask_batch[transl_nr])

                    for flip_nr in range(len(flipped_mask_batch)):
                        np.save(os.path.join(os.path.join(DATA_PATHx, 'data_frames_lesion'),
                                             str(patient_code) + '_' + str(case_nr) + '_l_flip' + str(
                                                 flip_nr) + '_' + str(
                                                 lesion_indexes[i]) + '_' + str(glo_index)), flipped_mask_batch[flip_nr])

                    # np.save(os.path.join(os.path.join(DATA_PATHx, 'data_frames_lesion'),
                    #                      str(patient_code) + '_' + str(case_nr) + '_l_rotz_' + str(
                    #                          lesion_indexes[i]) + '_' + str(glo_index)), mask_rz[i])
                    # np.save(os.path.join(os.path.join(DATA_PATHx, 'data_frames_lesion'),
                    #                      str(patient_code) + '_' + str(case_nr) + '_l_roty_' + str(
                    #                          lesion_indexes[i]) + '_' + str(glo_index)), mask_ry[i])
                    # np.save(os.path.join(os.path.join(DATA_PATHx, 'data_frames_lesion'),
                    #                      str(patient_code) + '_' + str(case_nr) + '_l_rotx_' + str(
                    #                          lesion_indexes[i]) + '_' + str(glo_index)), mask_rx[i])
                    #
                    # np.save(os.path.join(os.path.join(DATA_PATHx, 'data_frames_lesion'),
                    #                      str(patient_code) + '_' + str(case_nr) + '_l_flipz_' + str(
                    #                          lesion_indexes[i]) + '_' + str(glo_index)), mask_fz[i])
                    # np.save(os.path.join(os.path.join(DATA_PATHx, 'data_frames_lesion'),
                    #                      str(patient_code) + '_' + str(case_nr) + '_l_flipy_' + str(
                    #                          lesion_indexes[i]) + '_' + str(glo_index)), mask_fy[i])
                    # np.save(os.path.join(os.path.join(DATA_PATHx, 'data_frames_lesion'),
                    #                      str(patient_code) + '_' + str(case_nr) + '_l_flipx_' + str(
                    #                          lesion_indexes[i]) + '_' + str(glo_index)), mask_fx[i])

                    if i == 0:
                        print("shape of samples: ", res_arr_pet[lesion_indexes[i]].shape)
                    # save using the format case-nr_img-index_glob-nr
                    np.save(os.path.join(os.path.join(DATA_PATHx, 'data_frames_pet'),
                                         str(patient_code) + '_' + str(case_nr) + '_l_' + str(
                                             lesion_indexes[i]) + '_' + str(glo_index)),
                            rescale(res_arr_pet[lesion_indexes[i]], max_pet, min_pet))
                    np.save(os.path.join(os.path.join(DATA_PATHx, 'data_frames_ct'),
                                         str(patient_code) + '_' + str(case_nr) + '_l_' + str(
                                             lesion_indexes[i]) + '_' + str(glo_index)),
                            rescale(res_arr_ct[lesion_indexes[i]], max_ct, min_ct))
                    np.save(os.path.join(os.path.join(DATA_PATHx, 'data_frames_lesion'),
                                         str(patient_code) + '_' + str(case_nr) + '_l_' + str(
                                             lesion_indexes[i]) + '_' + str(glo_index)), res_arr_les[lesion_indexes[i]])

                    np.save(os.path.join(os.path.join(DATA_PATHx, 'data_frames_pet'),
                                         str(patient_code) + '_' + str(case_nr) + '_nl_' + str(
                                             sample_nonlesion_indexes[i]) + '_' + str(glo_index + 1)),
                            rescale(res_arr_pet[sample_nonlesion_indexes[i]], max_pet, min_pet))
                    np.save(os.path.join(os.path.join(DATA_PATHx, 'data_frames_ct'),
                                         str(patient_code) + '_' + str(case_nr) + '_nl_' + str(
                                             sample_nonlesion_indexes[i]) + '_' + str(glo_index + 1)),
                            rescale(res_arr_ct[sample_nonlesion_indexes[i]], max_ct, min_ct))
                    np.save(os.path.join(os.path.join(DATA_PATHx, 'data_frames_lesion'),
                                         str(patient_code) + '_' + str(case_nr) + '_nl_' + str(
                                             sample_nonlesion_indexes[i]) + '_' + str(glo_index + 1)),
                            res_arr_les[sample_nonlesion_indexes[i]])

                    glo_index += 2


                    #
                #
            # img = sitk.GetImageFromArray(nda)
            # "" ""


# PATH_STORAGE = "../NPY_data_rescaled"
#
# if not os.path.exists(PATH_STORAGE):
#     os.makedirs(PATH_STORAGE)
#
# for (root, dirs, files) in os.walk(r"C:\Users\admin\Downloads\VUB\Thesis MACS\Soft Work Practice\EtroDataset",
#                                    topdown=True):
#
#     if "case" in root and "Lesions.nii.gz" in files:
#
#         case_nr = root.split("_")[-1]
#         patient_ID = df.loc["case_" + case_nr, "Hash_encoded"]
#
#         # if the patient's ID is not in test set, skip
#         if patient_ID not in test:
#             continue
#
#         if int(case_nr) not in [64, 149]:
#             print("------------------------------------------------------")
#             # print("Working on case #", case_nr)
#             # path of the target case
#             data_path = "case_" + case_nr
#
#             # patient_code = df.loc[data_path,"Hash_encoded"]
#             print("Working on case #", case_nr, " -- patient $", patient_ID)
#
#             # open the resampled images file
#             res_img_pet = sitk.ReadImage(os.path.join(data_path, 'PET_img_resampled2.nii.gz'))
#             res_img_ct = sitk.ReadImage(os.path.join(data_path, 'CT_img_resampled2.nii.gz'))
#             res_img_lesions = sitk.ReadImage(os.path.join(data_path, 'Lesions_img_resampled2.nii.gz'))
#
#             # get the size of the image
#             pet_size = res_img_pet.GetSize()
#             ct_size = res_img_ct.GetSize()
#             les_size = res_img_lesions.GetSize()
#             # check if there is a difference between the number for pet & ct
#             # print(pet_size, ct_size, les_size)
#
#             # get the number of patches
#             nr_patches_pet = int(np.prod(list(pet_size)) / 32768)
#             nr_patches_ct = int(np.prod(list(ct_size)) / 32768)
#             nr_patches_lesions = int(np.prod(list(les_size)) / 32768)
#             print('nr of patches lesion: ', nr_patches_lesions)
#
#             # transform to numpy array
#             res_arr_pet = sitk.GetArrayFromImage(res_img_pet)
#             res_arr_ct = sitk.GetArrayFromImage(res_img_ct)
#             res_arr_lesions = sitk.GetArrayFromImage(res_img_lesions)
#             print()
#             '''
#             if case_nr == '1':
#                 plt.imshow(res_arr_pet[264,:,:], cmap=plt.cm.gray)
#             '''
#             # get the size of the numpy array
#             pet_shape = res_arr_pet.shape
#             ct_shape = res_arr_ct.shape
#             les_shape = res_arr_lesions.shape
#             # print(pet_shape, ct_shape, les_shape)
#
#             # if int(case_nr) in [1]:
#             #     plt.imshow(res_arr_pet[:, 64, :], cmap=plt.cm.gray)
#             '''
#             # change the channel ordering
#             res_arr_pet2 = moveaxis(res_arr_pet, 0, 2)
#             res_arr_pet2 = moveaxis(res_arr_pet2, 0, 1)
#             # ********************************************
#             res_arr_ct2 = moveaxis(res_arr_ct, 0, 2)
#             res_arr_ct2 = moveaxis(res_arr_ct2, 0, 1)
#             # ********************************************
#             res_arr_lesions2 = moveaxis(res_arr_lesions, 0, 2)
#             res_arr_lesions2 = moveaxis(res_arr_lesions2, 0, 1)
#             '''
#             # extract the patches
#             res_arr_pet_long = view_as_blocks(res_arr_pet, block_shape=(32, 32, 32))
#             res_arr_ct_long = view_as_blocks(res_arr_ct, block_shape=(32, 32, 32))
#             res_arr_lesions_long = view_as_blocks(res_arr_lesions, block_shape=(32, 32, 32))
#
#             ## check the size of the outputed array
#             ##
#             arr_pet = []
#             (Ip, Jp, Kp) = res_arr_pet_long.shape[:3]
#             for k in range(Kp):
#                 for j in range(Jp):
#                     for i in range(Ip):
#                         arr_pet.append(res_arr_pet_long[i, j, k, :, :, :])
#             res_arr_pet_long = np.array(arr_pet)
#
#             arr_ct = []
#             (Ic, Jc, Kc) = res_arr_ct_long.shape[:3]
#             for k in range(Kc):
#                 for j in range(Jc):
#                     for i in range(Ic):
#                         arr_ct.append(res_arr_ct_long[i, j, k, :, :, :])
#             res_arr_ct_long = np.array(arr_ct)
#
#             arr_les = []
#             (Il, Jl, Kl) = res_arr_lesions_long.shape[:3]
#             for k in range(Kl):
#                 for j in range(Jl):
#                     for i in range(Il):
#                         arr_les.append(res_arr_lesions_long[i, j, k, :, :, :])
#             res_arr_lesions_long = np.array(arr_les)
#
#             # reshape the array: number of patches to nr_patches*32*32*32
#             '''
#             res_arr_pet_long = res_arr_pet_long.reshape((nr_patches_pet, 32, 32, 32))
#             res_arr_ct_long = res_arr_ct_long.reshape((nr_patches_ct, 32, 32, 32))
#             res_arr_lesions_long = res_arr_lesions_long.reshape((nr_patches_lesions, 32, 32, 32))
#             ##'''
#             # check the patient's ID
#             # print(data_path)
#             path_to_patient = "Patient_" + str(df.loc[data_path, "Hash_encoded"])
#
#             # path_to_patient = "Patient_00"
#             # check if the patient's file exists already # otherwise, create the file
#             patient_loc = os.path.join(PATH_STORAGE, path_to_patient)
#             if not os.path.exists(patient_loc):
#                 os.makedirs(patient_loc)
#             """
#             # check if a file exists in the subdirectory for this patient's case# otherwise create a file for this case
#             case_loc = os.path.join(patient_loc, data_path)
#             if not os.path.exists(case_loc):
#                 os.makedirs(case_loc)
#             """
#
#             # store the .npy files
#             # save using the format case-nr_img-index_glob-nr
#             pet_patient_loc = os.path.join(patient_loc, "PET")
#             if not os.path.exists(pet_patient_loc):
#                 os.makedirs(pet_patient_loc)
#             rescaled_pet = rescale(res_arr_pet_long, max_pet, min_pet)
#             np.save(os.path.join(pet_patient_loc, str(data_path)), res_arr_pet_long)
#
#             ct_patient_loc = os.path.join(patient_loc, "CT")
#             if not os.path.exists(ct_patient_loc):
#                 os.makedirs(ct_patient_loc)
#             rescaled_ct = rescale(res_arr_ct_long, max_ct, min_ct)
#             np.save(os.path.join(ct_patient_loc, str(data_path)), res_arr_ct_long)
#
#             les_patient_loc = os.path.join(patient_loc, "LESION")
#             if not os.path.exists(les_patient_loc):
#                 os.makedirs(les_patient_loc)
#             # no rescaling needed for the mask
#             np.save(os.path.join(les_patient_loc, str(data_path)), res_arr_lesions_long)
#     '''if '1' in root.split("_")[-1]:
#         break
#     #'''