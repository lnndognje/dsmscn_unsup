"""
Created by Ine Dirks on 14/11/2020

"""

import SimpleITK as sitk
from math import pi
import numpy as np


def get_center(img):

    width, height, depth = img.GetSize()

    return img.TransformIndexToPhysicalPoint((int(np.ceil(width/2)),
                                              int(np.ceil(height/2)),
                                              int(np.ceil(depth/2))))


def resample(image, transform):
    reference_image = image
    interpolator = sitk.sitkLinear
    default_value = 0.

    return sitk.Resample(image, reference_image, transform, interpolator, default_value)


def rotate_patch(array, theta):
    image = sitk.GetImageFromArray(array)

    theta_x = theta[0] * pi / 180
    theta_y = theta[1] * pi / 180
    theta_z = theta[2] * pi / 180

    euler_transform = sitk.Euler3DTransform(get_center(image), theta_x, theta_y, theta_z, (0, 0, 0))
    euler_transform.SetCenter(get_center(image))
    euler_transform.SetRotation(theta_x, theta_y, theta_z)
    image_transformed = resample(image, euler_transform)

    return sitk.GetArrayFromImage(image_transformed)


def translate_patch(array, translation):
    image = sitk.GetImageFromArray(array)

    coordinates = get_center(image)
    # coordinates = image.TransformIndexToPhysicalPoint(coordinates)

    euler_transform = sitk.Euler3DTransform()
    resampler = sitk.ResampleImageFilter()
    resampler.SetInterpolator(sitk.sitkLinear)
    euler_transform.SetTranslation(translation)
    euler_transform.SetCenter(coordinates)
    resampler.SetTransform(euler_transform)
    resampler.SetSize(image.GetSize())
    resampler.SetOutputSpacing(image.GetSpacing())
    resampler.SetOutputOrigin(image.GetOrigin())
    image_transformed = resampler.Execute(image)

    return sitk.GetArrayFromImage(image_transformed)


def flip_patch(array, flip):
    image = sitk.GetImageFromArray(array)

    flipper = sitk.FlipImageFilter()
    flipper.SetFlipAxes(flip)
    image_transformed = flipper.Execute(image)
    image_transformed.CopyInformation(image)

    return sitk.GetArrayFromImage(image_transformed)


def augment_data(array):
    rotated_arrays = []
    translated_arrays = []
    flipped_arrays = []

    rotations = [(0, 0, 90), (0, 0, 180), (0, 0, 270),
                 (0, 90, 0), (0, 180, 0), (0, 270, 0),
                 (90, 0, 0), (180, 0, 0), (270, 0, 0)]

    translations = [(5, 0, 0), (-5, 0, 0),
                    (0, 5, 0), (0, -5, 0),
                    (0, 0, 5), (0, 0, -5)]

    flips = [(False, False, True), (False, True, False),
             (True, False, False), (True, True, True)]  # (False, True, True), (True, True, False), (True, False, True)]

    # for i in range(len(rotations)):
    #     rotated_arrays.append(rotate_patch(array, rotations[i]))

    for i in range(len(translations)):
        translated_arrays.append(translate_patch(array, translations[i]))

    # for i in range(len(flips)):
    #     flipped_arrays.append(flip_patch(array, flips[i]))

    return rotated_arrays, translated_arrays, flipped_arrays

