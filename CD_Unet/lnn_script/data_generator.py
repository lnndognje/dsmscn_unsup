"""
Created by Ine Dirks on 15/11/2020

"""

import keras
import numpy as np
import random


class DataGenerator(keras.utils.Sequence):
    def __init__(self, list_ids, list_ids_mask, batch_size, dim, n_channels=1,
                 n_classes=1, shuffle=False):
        """ Initialization """
        self.dim = dim
        self.batch_size = batch_size
        self.list_ids = list_ids
        self.list_ids_mask = list_ids_mask
        self.n_channels = n_channels
        self.n_classes = n_classes
        self.shuffle = shuffle
        self.on_epoch_end()

    def __len__(self):
        """ Denotes the number of batches per epoch """
        return int(np.floor(len(self.list_ids) / self.batch_size))

    def __getitem__(self, index):
        """ Generate one batch of data """
        # Generate indexes of the batch
        indexes = self.indexes[index * self.batch_size:(index + 1) * self.batch_size]

        # Find list of IDs
        list_ids_temp = [self.list_ids[k] for k in indexes]
        list_ids_temp_mask = [self.list_ids_mask[k] for k in indexes]
        x, y = self.__data_generation(list_ids_temp, list_ids_temp_mask)

        return x, y

    def on_epoch_end(self):
        """ Updates indexes after each epoch """
        self.indexes = np.arange(len(self.list_ids))
        if self.shuffle:
            np.random.shuffle(self.indexes)

    def __data_generation(self, list_IDs_temp, list_IDs_mask):
        """ Generates data containing batch_size samples """
        # Initialization
        x = np.empty((self.batch_size, self.dim[0], self.dim[1], self.dim[2], self.n_channels))
        y = np.empty((self.batch_size, self.dim[0], self.dim[1], self.dim[2], self.n_channels))

        # Generate data
        for i, ID in enumerate(list_IDs_temp):
            # Store sample
            array_im = np.load(ID)
            array_mask = np.load(list_IDs_mask[i])

            x[i, ] = np.reshape(array_im, [self.dim[0], self.dim[1], self.dim[2], 1])

            # Store class
            y[i, ] = np.reshape(array_mask, [self.dim[0], self.dim[1], self.dim[2], 1])

        return x, y


class DataGenerator2Inputs(keras.utils.Sequence):
    def __init__(self, list_ids1, list_ids2, list_ids_mask, batch_size, dim, n_channels=1,
                 n_classes=1, shuffle=False):
        """ Initialization """
        self.dim = dim
        self.batch_size = batch_size
        self.list_ids1 = list_ids1
        self.list_ids2 = list_ids2
        self.list_ids_mask = list_ids_mask
        self.n_channels = n_channels
        self.n_classes = n_classes
        self.shuffle = shuffle
        self.on_epoch_end()

    def __len__(self):
        """ Denotes the number of batches per epoch """
        return int(np.floor(len(self.list_ids1) / self.batch_size))

    def __getitem__(self, index):
        """ Generate one batch of data """
        # Generate indexes of the batch
        indexes = self.indexes[index * self.batch_size:(index + 1) * self.batch_size]

        # Find list of IDs
        list_ids_temp1 = [self.list_ids1[k] for k in indexes]
        list_ids_temp2 = [self.list_ids2[k] for k in indexes]
        list_ids_temp_mask = [self.list_ids_mask[k] for k in indexes]
        x, y = self.__data_generation(list_ids_temp1, list_ids_temp2, list_ids_temp_mask)

        return x, y

    def on_epoch_end(self):
        """ Updates indexes after each epoch """
        self.indexes = np.arange(len(self.list_ids1))
        if self.shuffle:
            np.random.shuffle(self.indexes)

    def __data_generation(self, list_IDs_temp1, list_IDs_temp2, list_IDs_mask):
        """ Generates data containing batch_size samples """
        # Initialization
        x1 = np.empty((self.batch_size, self.dim[0], self.dim[1], self.dim[2], self.n_channels))
        x2 = np.empty((self.batch_size, self.dim[0], self.dim[1], self.dim[2], self.n_channels))
        y = np.empty((self.batch_size, self.dim[0], self.dim[1], self.dim[2], self.n_channels))

        # Generate data
        for i, ID in enumerate(list_IDs_temp1):
            # Store sample
            array_im1 = np.load(ID)
            array_im2 = np.load(list_IDs_temp2[i])
            array_mask = np.load(list_IDs_mask[i])

            x1[i, ] = np.reshape(array_im1, [self.dim[0], self.dim[1], self.dim[2], 1])
            x2[i, ] = np.reshape(array_im2, [self.dim[0], self.dim[1], self.dim[2], 1])

            # Store class
            y[i, ] = np.reshape(array_mask, [self.dim[0], self.dim[1], self.dim[2], 1])

        return [x1, x2], y

