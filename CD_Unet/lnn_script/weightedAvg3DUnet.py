import numpy as np 
import os
from math import ceil
import numpy as np
import cv2
from scipy import stats
import random
from math import sqrt
from keras import backend as K
from keras.models import *
from keras.layers import *
from keras.optimizers import *
from keras.callbacks import *
from keras import backend as keras
import tensorflow as tf


import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt

# call/import self-made modules
from math import ceil

os.environ['CUDA_VISIBLE_DEVICES'] = '2'
# to check the usage of the gpu: nvidia-smi


# lamda = 1.0
# epsilon = 1e-6
#
def dice_coef(y_true, y_pred, smooth=1e-7):
    y_true_f = K.flatten(y_true)
    y_pred_f = K.flatten(y_pred)
    intersection = K.sum(y_true_f * y_pred_f)
    return (2. * intersection + smooth) / (K.sum(y_true_f) + K.sum(y_pred_f) + smooth)


def dice_coef_loss(y_true, y_pred):
    return 1-dice_coef(y_true, y_pred)
#
#
# def focal_loss(y_true,y_pred,gamma=0.,alpha=0.5):
#     # pdb.set_trace()
#     y_pred=	K.clip(y_pred,epsilon,1.-epsilon)
#     pt_1=tf.where(tf.equal(y_true,1),y_pred,tf.ones_like(y_pred))
#     pt_0=tf.where(tf.equal(y_true,0),y_pred,tf.zeros_like(y_pred))
#     pt_1 = K.clip(pt_1, epsilon, 1. - epsilon)
#     pt_0 = K.clip(pt_0, epsilon, 1. - epsilon)
#     return -K.mean(alpha * K.pow(1. - pt_1,gamma)*K.log(pt_1))-K.mean((1-alpha) * K.pow( pt_0,gamma)*K.log(1. - pt_0))
#
#
# def custom_loss(y_true, y_pred):
#     #  combination of focal loss and dice coef loss
#     return dice_coef_loss(y_true, y_pred) + lamda*focal_loss(y_true,y_pred)
#
#
# def sensitivity_coef(y_true, y_pred, smooth=1e-6):
#     y_true_f = K.flatten(y_true)
#     y_pred_f = K.flatten(y_pred)
#     intersection = K.sum(y_true_f * y_pred_f)
#     return (intersection + smooth) / (K.sum(y_true_f) + smooth)
#
#
# def sensitivity_coef_loss(y_true, y_pred):
#     return 1 - sensitivity_coef(y_true, y_pred)
#
#
# def mean_absolute_error(y_true, y_pred):
#     y_true_f = K.flatten(y_true)
#     y_pred_f = K.flatten(y_pred)
#     return K.mean(K.abs((y_pred_f - y_true_f)))
#
#
# def custom_loss2(y_true, y_pred):
#     #  combination of mean absolute error, sensitivity and dice coef loss
#
#     return dice_coef_loss(y_true, y_pred) + sensitivity_coef_loss(y_true, y_pred) + mean_absolute_error(y_true,y_pred)

# Define the custom layer

class WeightedSum(Layer):
    """A custom keras layer to learn a weighted sum of tensors"""

    def __init__(self, **kwargs):
        super(WeightedSum, self).__init__(**kwargs)

    def build(self, input_shape=1):
        self.a = self.add_weight(
            name='alpha',
            shape=(),
            initializer='ones',
            constraint=lambda x: tf.clip_by_value(x, 0, 1), #constraint=tf.keras.constraints.min_max_norm(max_value=1,min_value=0),
            dtype='float32',
            trainable=True,
        )
        super(WeightedSum, self).build(input_shape)

    def call(self, model_outputs):
        return self.a * model_outputs[0] + (1 - self.a) * model_outputs[1]

    def compute_output_shape(self, input_shape):
        return input_shape[0]


def create_unet(inputx):
    
    conv1 = Conv3D(16, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(inputx)
    conv1 = Conv3D(16, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv1)
    conv1 = BatchNormalization()(conv1)
    pool1 = MaxPooling3D(pool_size=(2, 2, 2))(conv1)
    conv2 = Conv3D(32, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(pool1)
    conv2 = Conv3D(32, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv2)
    conv2 = BatchNormalization()(conv2)
    pool2 = MaxPooling3D(pool_size=(2, 2, 2))(conv2)
    conv3 = Conv3D(64, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(pool2)
    conv3 = Conv3D(64, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv3)
    conv3 = BatchNormalization()(conv3)
    drop3 = Dropout(0.5)(conv3) # rate = 0.5
    pool3 = MaxPooling3D(pool_size=(2, 2, 2))(drop3)
    
    conv4 = Conv3D(128, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(pool3)
    conv4 = Conv3D(128, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv4)
    conv4 = BatchNormalization()(conv4)
    drop4 = Dropout(0.5)(conv4)
    
    pool4 = MaxPooling3D(pool_size=(2, 2, 2))(conv4)

    up7 = Conv3D(64, 2, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(UpSampling3D(size = (2,2,2))(drop4))
    merge7 = Concatenate(axis=-1)([conv3,up7])
    conv7 = Conv3D(64, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(merge7)
    conv7 = Conv3D(64, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv7)
    conv7 = BatchNormalization()(conv7)
    
    up8 = Conv3D(32, 2, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(UpSampling3D(size = (2,2,2))(conv7))
    merge8 = Concatenate(axis=-1)([conv2,up8])
    conv8 = Conv3D(32, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(merge8)
    conv8 = Conv3D(32, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv8)

    conv8 = BatchNormalization()(conv8)
    up9 = Conv3D(16, 2, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(UpSampling3D(size = (2,2,2))(conv8))
    
    merge9 = Concatenate(axis=-1)([conv1, up9])
    conv9 = Conv3D(16, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(merge9)
    conv9 = Conv3D(16, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv9)
    conv9 = Conv3D(2, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv9)
    conv10 = Conv3D(1, 1, activation = 'sigmoid')(conv9)
    return conv10


def double1unet(input_size1=(32,32,32,1), input_size2=(32,32,32,1)):
    
    # 
    input1 = Input(shape=input_size1)
    input2 = Input(shape=input_size2)
    
    # 
    branch1_output = create_unet(input1)
    branch2_output = create_unet(input2)
    
    #
    model1 = Model(input1, branch1_output)
    model2 = Model(input2, branch2_output)
    
    # Weighed sum of the two models' outputs with a = 0.1
    out = WeightedSum()([model1.output, model2.output])
    
    # Create the merged model
    # model = Model(inputs=[inp1, inp2], outputs=[out])
    model = Model(input=[input1, input2], output=out)

    # 
    model.compile(optimizer=Adam(lr=1e-4), loss=dice_coef_loss, metrics=[dice_coef])
    # model.compile(optimizer=Adam(lr=1e-4), loss=custom_loss, metrics=[dice_coef])
    #model.compile(optimizer=Adam(lr=1e-4), loss=custom_loss2, metrics=[dice_coef])
    model.summary()
    
    return model

# Free up RAM in case the model definition cells were run multiple times
# K.clear_session()

# initialize self-designed image generator
""" changed this """
ROOT = "../Dataset_rescaledzx0_new" #"../datasetx"
# ROOT = "/Volumes/Passport/Laurent/Dataset_rescaledzx0"  # "../datasetx"
# ROOT = "../Dataset_rescaledzx0" #"../datasetx"
# ROOT = "../../laurent/Dataset_rescaledzx0" #"../datasetx"
DATA_PATH = os.path.join(ROOT, 'train')
DATA_PATH_VALID = os.path.join(ROOT, 'validation')

""" added this """
batch_size = 32

train_frame_pet_path = os.path.join(DATA_PATH, 'data_frames_pet')
train_frame_ct_path = os.path.join(DATA_PATH, 'data_frames_ct')
train_frame_lesion_path = os.path.join(DATA_PATH, 'data_frames_lesion')

val_frame_pet_path = os.path.join(DATA_PATH_VALID, 'data_frames_pet')
val_frame_ct_path = os.path.join(DATA_PATH_VALID, 'data_frames_ct')
val_frame_lesion_path = os.path.join(DATA_PATH_VALID, 'data_frames_lesion')

#*****************************************************************
""" included this to use .fit"""

n = os.listdir(train_frame_pet_path)
img_pet_train = []
img_ct_train = []
mask_train = []
for i in range(len(n)):
    img_pet_train.append(np.load(train_frame_pet_path + '/' + n[i]))
    img_ct_train.append(np.load(train_frame_ct_path + '/' + n[i]))
    mask_train.append(np.load(train_frame_lesion_path + '/' + n[i]))

n = os.listdir(val_frame_pet_path)
img_pet_val = []
img_ct_val = []
mask_val = []
for i in range(len(n)):
    img_pet_val.append(np.load(val_frame_pet_path + '/' + n[i]))
    img_ct_val.append(np.load(val_frame_ct_path + '/' + n[i]))
    mask_val.append(np.load(val_frame_lesion_path + '/' + n[i]))

nr_training_im = len(img_pet_train)
nr_val_im = len(img_pet_val)

img_pet_train = np.reshape(np.asarray(img_pet_train), [nr_training_im, 32, 32, 32, 1])
img_ct_train = np.reshape(np.asarray(img_ct_train), [nr_training_im, 32, 32, 32, 1])
mask_train = np.reshape(np.asarray(mask_train), [nr_training_im, 32, 32, 32, 1])
img_pet_val = np.reshape(np.asarray(img_pet_val), [nr_val_im, 32, 32, 32, 1])
img_ct_val = np.reshape(np.asarray(img_ct_val), [nr_val_im, 32, 32, 32, 1])
mask_val = np.reshape(np.asarray(mask_val), [nr_val_im, 32, 32, 32, 1])

print('train PET:  {}'.format(img_pet_train.shape))
print('train CT:   {}'.format(img_ct_train.shape))
print('train mask: {}'.format(mask_train.shape))
print()
print('val PET:  {}'.format(img_pet_val.shape))
print('val CT:   {}'.format(img_ct_val.shape))
print('val mask: {}'.format(mask_val.shape))

checkpoint = ModelCheckpoint('unetweightavg_valloss_aug_ep400_b32_2.hdf5', monitor='val_loss', verbose=1,
                             save_best_only=True)
#
csv_logger = CSVLogger('./unetweightavg_valloss_aug_ep400_b32_2_log.out', append=True, separator=';')
#
reduce_lr = ReduceLROnPlateau(factor=0.1, patience=5, min_lr=0.001, verbose=1)
#
callbacks_list = [checkpoint, csv_logger, reduce_lr]

# fit generators to the built model
model = double1unet()
#model = unet((32, 32, 32, 1))
segmenter = model.fit([img_pet_train, img_ct_train], mask_train, validation_data=([img_pet_val, img_ct_val], mask_val), batch_size=batch_size,
                      epochs=400, callbacks=callbacks_list)

#*****************************************************************
""" changed this to use new generator 
n = os.listdir(train_frame_pet_path)
img_pet_train_names = []
img_ct_train_names = []
mask_train_names = []
for i in range(len(n)):
    img_pet_train_names.append(train_frame_pet_path + '/' + n[i])
    img_ct_train_names.append(train_frame_ct_path + '/' + n[i])
    mask_train_names.append(train_frame_lesion_path + '/' + n[i])

n = os.listdir(val_frame_pet_path)
img_pet_val_names = []
img_ct_val_names = []
mask_val_names = []
for i in range(len(n)):
    img_pet_val_names.append(val_frame_pet_path + '/' + n[i])
    img_ct_val_names.append(val_frame_ct_path + '/' + n[i])
    mask_val_names.append(val_frame_lesion_path + '/' + n[i])

nr_training_im = len(img_pet_train_names)
nr_val_im = len(img_pet_val_names)

from sklearn.utils import shuffle
img_pet_train_names, img_ct_train_names, mask_train_names = shuffle(img_pet_train_names, img_ct_train_names, mask_train_names, random_state=2)
img_pet_val_names, img_ct_val_names, mask_val_names = shuffle(img_pet_val_names, img_ct_val_names, mask_val_names, random_state=2)

# print(img_pet_train_names[50])
# print(img_ct_train_names[50])
# print(mask_train_names[50])

from data_generator import DataGenerator2Inputs
train_gen = DataGenerator2Inputs(list_ids1=img_pet_train_names, list_ids2=img_ct_train_names,
                                 list_ids_mask=mask_train_names, batch_size=batch_size, dim=(32, 32, 32))
val_gen = DataGenerator2Inputs(list_ids1=img_pet_val_names, list_ids2=img_ct_val_names,
                               list_ids_mask=mask_val_names, batch_size=batch_size, dim=(32, 32, 32))

checkpoint = ModelCheckpoint('duoweightavg_valloss_aug_rescaledpetct3_ep300_b64_2_lr03.hdf5', monitor='val_loss', verbose=1,
                             save_best_only=True)
#
csv_logger = CSVLogger('./duoweightavg_valloss_aug_rescaledpetct3_ep300_b64_2_lr03_log.out', append=True, separator=';')
#
reduce_lr = ReduceLROnPlateau(factor=0.2, patience=3, min_lr=0.001, verbose=1)
#
callbacks_list = [checkpoint, csv_logger, reduce_lr]

model = double1unet()
print('Training:   {}'.format(nr_training_im))
print('Validation: {}'.format(nr_val_im))

segmenter = model.fit_generator(train_gen, steps_per_epoch=nr_training_im//batch_size, validation_data=val_gen,
                                validation_steps=nr_val_im//batch_size, epochs=300, callbacks=callbacks_list)
"""
""" end of changed this to use new generator """

""" this was added """
import pickle
# Save the history object
with open('./historyx', 'wb') as file_pi:
    pickle.dump(segmenter.history, file_pi)

### result viewer
dice = segmenter.history['dice_coef']
val_dice = segmenter.history['val_dice_coef']
loss = segmenter.history['loss']
val_loss = segmenter.history['val_loss']
epochs = range(1, len(dice) + 1)

# plt.plot(epochs, dice, 'bo', label='Training dice')
# plt.plot(epochs, val_dice, 'b', label='Validation dice')
plt.plot(epochs, dice, label='Training dice')
plt.plot(epochs, val_dice, label='Validation dice')
plt.title('Training and validation dice coefficient')
plt.legend()

""" changed this """
plt.savefig("unetweightavg_valloss_aug_ep400_b32_2_diceCoef.png")
plt.figure()

plt.plot(epochs, loss, label='Training loss')
plt.plot(epochs, val_loss, label='Validation loss')
plt.title('Training and validation loss')
plt.legend()

""" changed this """
plt.savefig("unetweightavg_valloss_aug_ep400_b32_2_loss.png")
# plt.show()

